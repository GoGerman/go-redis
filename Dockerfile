FROM golang:1.20.2-alpine

RUN go version

ENV GOPATH=/

COPY . .

RUN go mod download

RUN go build -o main .

CMD ["./main"]
