package app

import (
	"github.com/go-redis/redis/v8"
	"gitlab.com/GoGerman/go-parser/internal/config"
	"gitlab.com/GoGerman/go-parser/internal/database"
	"gitlab.com/GoGerman/go-parser/internal/modules/handlers"
	"gitlab.com/GoGerman/go-parser/internal/modules/repository"
	"gitlab.com/GoGerman/go-parser/internal/modules/service"
	"gitlab.com/GoGerman/go-parser/internal/router"
	"log"
)

func Run() {
	conf := config.DB{
		Host:     "postgres",
		Port:     "5432",
		Password: "1234",
		User:     "user",
		Name:     "postgres",
		Driver:   "postgres",
	}
	client := redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	//conf2 := config.DB{
	//	Host:     "mongo",
	//	Port:     "27017",
	//	Password: "1234",
	//	User:     "user",
	//	Name:     "mongo",
	//	Driver:   "mongo",
	//}
	postg, mongo, err := database.NewDB(conf)
	if err != nil {
		log.Fatalln(err)
	}
	repo := repository.NewVacancyRepository(postg, mongo, client)
	serv := service.NewVacancyService(repo)
	cont := handlers.NewVacancyController(serv)
	router.RunServer(cont)
}
