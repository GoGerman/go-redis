package handlers

import (
	"context"
	"encoding/json"
	"github.com/go-chi/chi"
	"gitlab.com/GoGerman/go-parser/internal/models"
	"net/http"
	"strconv"
	"time"
)

type Parser interface {
	Create(ctx context.Context, lang string) error
	GetById(ctx context.Context, id int) (models.Vacancy, error)
	GetList(ctx context.Context) ([]models.Vacancy, error)
	Delete(ctx context.Context, id int) error
}

type VacancyController struct {
	Data Parser
}

func NewVacancyController(data Parser) *VacancyController {
	return &VacancyController{Data: data}
}

func (v *VacancyController) List(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	list, err := v.Data.GetList(ctx)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")

	err = json.NewEncoder(w).Encode(list)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (v *VacancyController) Get(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	num, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	data, err := v.Data.GetById(r.Context(), num)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")

	err = json.NewEncoder(w).Encode(data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (v *VacancyController) Delete(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	num, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = v.Data.Delete(r.Context(), num)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
}

func (v *VacancyController) Search(w http.ResponseWriter, r *http.Request) {
	lang := chi.URLParam(r, "lang")

	err := v.Data.Create(r.Context(), lang)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")

	err = json.NewEncoder(w).Encode(err)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
