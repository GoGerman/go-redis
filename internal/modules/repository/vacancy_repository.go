package repository

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
	"gitlab.com/GoGerman/go-parser/internal/models"
	"gitlab.com/GoGerman/go-parser/internal/modules/selenium"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

const (
	Query = `INSERT INTO vacancies (
		type, date_posted, title, description,
		identifier_type, identifier_name, identifier_value, valid_through,
        hiring_organization_type, hiring_organization_name, hiring_organization_logo, hiring_organization_same_as
	) VALUES (
		$1, $2, $3, $4, $5, $6, $7, $8, $9, $10,
		$11, $12
	)
`
)

type Parser interface {
	Create(ctx context.Context, lang string) error
	GetById(ctx context.Context, id int) (models.Vacancy, error)
	GetList(ctx context.Context) ([]models.Vacancy, error)
	Delete(ctx context.Context, id int) error
}

type VacancyRepository struct {
	postgres *sqlx.DB
	mongo    *mongo.Collection
	redis    *redis.Client
	count    int
}

func NewVacancyRepository(postgres *sqlx.DB, mongo *mongo.Collection, redis *redis.Client) Parser {
	return &VacancyRepository{
		postgres: postgres,
		mongo:    mongo,
		redis:    redis,
		count:    0,
	}
}

func (v *VacancyRepository) Create(ctx context.Context, lang string) error {
	list := selenium.FirefoxDriverAndParser(lang)
	if v.postgres != nil {
		for i := range list {
			_, err := v.postgres.Exec(Query,
				list[i].Type,
				list[i].DatePosted, list[i].Title,
				list[i].Description, list[i].Identifier.Type,
				list[i].Identifier.Name, list[i].Identifier.Value,
				list[i].ValidThrough, list[i].HiringOrganization.Type, list[i].HiringOrganization.Name,
				list[i].HiringOrganization.Logo, list[i].HiringOrganization.SameAs,
			)
			if err != nil {
				log.Println(err)
			}
			v.count++
		}

		var row string
		query := "SELECT type FROM vacancies WHERE id= $1"
		var count int
		err := v.postgres.QueryRow("SELECT COUNT(*) FROM vacancies").Scan(&count)
		if err != nil {
			log.Println(err)
		}
		fmt.Printf("Количество записей в базе данных: %d\n", count)
		for i := 1; i <= count; i++ {
			err = v.postgres.Get(&row, query, i)
			if err != nil {
				log.Println(err)
			}
			if row == "" {
				err = v.Delete(ctx, i)
				if err != nil {
					log.Println(err)
				}
			}
		}

		err = v.postgres.QueryRow("SELECT COUNT(*) FROM vacancies").Scan(&count)
		if err != nil {
			log.Println(err)
		}
		log.Printf("обновленное количество записей в базе данных: %d\n", count)
		return nil
	} else if v.mongo != nil {
		for i := range list {
			sequenceDocument := bson.M{}
			change := options.FindOneAndUpdate().SetUpsert(true).SetReturnDocument(options.After)
			filter := bson.M{"_id": "vacancy_id_seq"}
			update := bson.M{"$inc": bson.M{"seq": 1}}

			err := v.mongo.FindOneAndUpdate(ctx, filter, update, change).Decode(&sequenceDocument)
			if err != nil {
				log.Println(err)
			}

			list[i].Id = sequenceDocument["seq"].(int)

			_, err = v.mongo.InsertOne(ctx, list[i])
			v.count++
			if err != nil {
				log.Println(err)
			}
		}
		return nil
	}

	return errors.New("can't create")
}

func (v *VacancyRepository) GetById(ctx context.Context, id int) (models.Vacancy, error) {
	var vacancy models.Vacancy
	if v.postgres != nil {
		query := "SELECT * FROM vacancies WHERE id = $1"
		err := v.postgres.QueryRow(query, id).Scan(
			&vacancy.Id, &vacancy.Type,
			&vacancy.DatePosted, &vacancy.Title,
			&vacancy.Description, &vacancy.Identifier.Type,
			&vacancy.Identifier.Name, &vacancy.Identifier.Value,
			&vacancy.ValidThrough, &vacancy.HiringOrganization.Type,
			&vacancy.HiringOrganization.Name,
			&vacancy.HiringOrganization.Logo, &vacancy.HiringOrganization.SameAs)
		if err != nil {
			return models.Vacancy{}, errors.New("wrong id")
		}
		jsonData, err := json.Marshal(vacancy)
		if err != nil {
			return models.Vacancy{}, errors.New("error in marshal")
		}
		err = v.redis.Set(ctx, string(rune(id)), jsonData, 0).Err()
		if err != nil {
			fmt.Println(err)
			return models.Vacancy{}, errors.New("error in redis cache")
		}
		return vacancy, nil
	} else if v.mongo != nil {
		err := v.mongo.FindOne(ctx, bson.M{"id": id}).Decode(&vacancy)
		if err != nil {
			if err == mongo.ErrNoDocuments {
				return vacancy, errors.New("wrong id")
			}
			jsonData, err := json.Marshal(vacancy)
			if err != nil {
				return models.Vacancy{}, errors.New("error in marshal")
			}
			err = v.redis.Set(ctx, string(rune(id)), jsonData, 0).Err()
			if err != nil {
				fmt.Println(err)
				return models.Vacancy{}, errors.New("error in redis cache")
			}
			return vacancy, nil
		}
	}
	return models.Vacancy{}, errors.New("can't get by id")
}

func (v *VacancyRepository) GetList(ctx context.Context) ([]models.Vacancy, error) {
	vacancies := make([]models.Vacancy, v.count)
	if v.postgres != nil {
		query := "SELECT * FROM vacancies"
		if rows, err := v.postgres.QueryContext(ctx, query); err != nil {
			return []models.Vacancy{}, err
		} else {
			defer rows.Close()
			for rows.Next() {
				vacancy := models.Vacancy{}
				if err := rows.Scan(
					&vacancy.Id, &vacancy.Type,
					&vacancy.DatePosted, &vacancy.Title,
					&vacancy.Description, &vacancy.Identifier.Type,
					&vacancy.Identifier.Name, &vacancy.Identifier.Value,
					&vacancy.ValidThrough, &vacancy.HiringOrganization.Type,
					&vacancy.HiringOrganization.Name,
					&vacancy.HiringOrganization.Logo, &vacancy.HiringOrganization.SameAs); err != nil {
					return []models.Vacancy{}, err
				}
				vacancies = append(vacancies, vacancy)
			}
			return vacancies, nil
		}
	} else if v.mongo != nil {
		cur, err := v.mongo.Find(ctx, bson.M{})
		if err != nil {
			return nil, err
		}
		defer cur.Close(ctx)
		for cur.Next(ctx) {
			var vacancy models.Vacancy
			err = cur.Decode(&vacancy)
			if err != nil {
				return nil, err
			}
			vacancies = append(vacancies, vacancy)
		}
		if err = cur.Err(); err != nil {
			return nil, err
		}
		if len(vacancies) == 0 {
			log.Println(errors.New("database in empty"))
		}
		return vacancies, nil
	}
	return []models.Vacancy{}, errors.New("can't get list")
}

func (v *VacancyRepository) Delete(ctx context.Context, id int) error {
	if v.postgres != nil {
		query := `DELETE FROM vacancies WHERE id = $1`
		if result, err := v.postgres.Exec(query, id); err != nil {
			return err
		} else {
			d, _ := result.RowsAffected()
			if d == 0 {
				return errors.New("wrong id")
			}
			err = v.redis.Del(ctx, string(rune(id))).Err()
			if err != nil {
				return errors.New("error in delete redis cache")
			}
			return nil
		}
	} else if v.mongo != nil {
		result, err := v.mongo.DeleteOne(ctx, bson.M{"id": id})
		if err != nil {
			return err
		}
		if result.DeletedCount == 0 {
			return errors.New("wrong id")
		}
		err = v.redis.Del(ctx, string(rune(id))).Err()
		if err != nil {
			return errors.New("error in delete redis cache")
		}
		return nil
	}
	return errors.New("can;t delete")
}
