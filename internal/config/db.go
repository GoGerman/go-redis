package config

type DB struct {
	Host     string
	Port     string
	User     string
	Password string
	Name     string
	Driver   string
}
